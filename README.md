# Fortunes REST API
REST API behaving like the popular Unix "fortune" program, delivering
fortune cookies.

Written in Python/Flask by Volker Kettenbach with Credits to Michael Goertz

## Live Demo
http://www.muppetlabs.de


## Installation
### Local

- `git clone https://gitlab.com/volkerkettenbach/fortunes-api`
- `virtualenv -p $(/usr/bin/env python3) venv`
- `source venv/bin/activate`
- `pip install --upgrade pip`
- `pip install -r requirements.txt`
- `python -m flask run`

### Docker
The sourcecode includes a Dockerfile and a docker-compose.yml. In the
later, please adjust the ip address (in case of using the macvlan network
model) or the exposed port to your needs. Then run:

`docker-compose up -d
`

and your done.


### Google App Engine
The app can be easily deployed to Google App Engine. You need to create
a project and set up everything needed to run software in Google App
Engine (GAE), which is beyond this document. Then just run:

`cp app.yaml.example app.yaml`

`gcloud app deploy`


## Google Analytics
In your runtime enviroment, you can set enviroment variable `GAID` to
your Google Analytics id. fortune-api will embedded the google analytics
tracking code into its webpages. Have a look at docker-compose.yml for
doing this for docker and app.yaml.example for Google App Engine.

## API Usage
Once installed, you can use the REST API as follows:

#### Retrieve a fortune cookie from any category and language

##### Request
`GET /api`

##### Response (application/json)
`{
 "fortune": "Never underestimate the power of somebody with source code, a text editor,
  and the willingness to totally hose their system.
     -- Rob Landley ",
"source": [
  "fortunes/de",
  "fortunes/en"
  ]
}`

#### Retrieve a fortune cookie from a specific category and language

##### Request
`GET /api/?lang=LANG&categories=CATEGORIES `

`LANG: One out of {de,en}`

`CATEGORIES: one to many categories as comma seperated list.`

For a list of valid combinations of language and category see below

#### Response (application/json)
Same as before


#### Valid combinations of language and category are


`"fortunes/de/anekdoten", "fortunes/de/asciiart", "fortunes/de/bahnhof",
"fortunes/de/beilagen", "fortunes/de/brot",
"fortunes/de/channel-debian.fortunes", "fortunes/de/computer",
"fortunes/de/debian", "fortunes/de/dessert", "fortunes/de/elefanten",
"fortunes/de/gedichte", "fortunes/de/hauptgericht",
"fortunes/de/holenlassen", "fortunes/de/huhn", "fortunes/de/infodrom",
"fortunes/de/kalt", "fortunes/de/kinderzitate", "fortunes/de/kuchen",
"fortunes/de/letzteworte", "fortunes/de/lieberals",
"fortunes/de/linuxtag", "fortunes/de/loewe", "fortunes/de/mathematiker",
"fortunes/de/ms", "fortunes/de/murphy", "fortunes/de/namen",
"fortunes/de/plaetzchen", "fortunes/de/quiz", "fortunes/de/regeln",
"fortunes/de/salat", "fortunes/de/sauce",
"fortunes/de/sicherheitshinweise", "fortunes/de/sprichworte",
"fortunes/de/sprichwortev", "fortunes/de/sprueche",
"fortunes/de/stilblueten", "fortunes/de/suppe", "fortunes/de/tips",
"fortunes/de/translations", "fortunes/de/unfug", "fortunes/de/vornamen",
"fortunes/de/vorspeise", "fortunes/de/warmduscher", "fortunes/de/witze",
"fortunes/de/woerterbuch", "fortunes/de/wusstensie",
"fortunes/de/zitate", "fortunes/en/art", "fortunes/en/ascii-art",
"fortunes/en/computers", "fortunes/en/cookie", "fortunes/en/debian",
"fortunes/en/definitions", "fortunes/en/disclaimer",
"fortunes/en/drugs", "fortunes/en/education", "fortunes/en/ethnic",
"fortunes/en/food", "fortunes/en/fortunes", "fortunes/en/goedel",
"fortunes/en/humorists", "fortunes/en/kids", "fortunes/en/knghtbrd",
"fortunes/en/law", "fortunes/en/linux", "fortunes/en/linuxcookie",
"fortunes/en/literature", "fortunes/en/love", "fortunes/en/magic",
"fortunes/en/medicine", "fortunes/en/men-women",
"fortunes/en/miscellaneous", "fortunes/en/news",
"fortunes/en/paradoxum", "fortunes/en/people", "fortunes/en/perl",
"fortunes/en/pets", "fortunes/en/platitudes", "fortunes/en/politics",
"fortunes/en/pratchett", "fortunes/en/riddles", "fortunes/en/science",
"fortunes/en/songs-poems", "fortunes/en/sports", "fortunes/en/startrek",
"fortunes/en/tao", "fortunes/en/translate-me", "fortunes/en/wisdom",
"fortunes/en/work", "fortunes/en/zippy"`


Get these as a list by querying

`GET /api/?categories=showvalid
`

## Source
Source code is available at https://gitlab.com/volkerkettenbach/fortunes-api

## License
see file LICENSE

## Credit
Credits go to Michael Goertz, for his python implementation of the unix fortune program
https://github.com/goerz/fortune.py