FROM python:3.7-alpine

LABEL maintainer="Volker Kettenbach <volker@vicosmo.de>"

WORKDIR /srv

COPY fortunes fortunes
COPY requirements.txt boot.sh fortune.py ./

RUN apk update \
    && apk upgrade \
    && apk add --no-cache curl python pkgconfig python-dev openssl-dev libffi-dev musl-dev make gcc \
    && python3 -m venv venv \
    && venv/bin/pip3 install --upgrade pip \
    && venv/bin/pip3 install -r requirements.txt \
    && venv/bin/pip3 install gunicorn \
    && chmod +x boot.sh

COPY static static
COPY templates templates
COPY main.py ./

ENV FLASK_APP app.py

# Nicht mit macvlan
# EXPOSE 5000
ENTRYPOINT ["./boot.sh"]