# Copyright: Volker Kettenbach, 2018
# For license see file LICENSE

# [START gae_python37_app]

from flask import Flask
from flask_restful import Resource, Api, reqparse
import fortune
from flask import render_template
import os

app = Flask(__name__)
api = Api(app)

app.config['FORTUNE_PATH'] = "fortunes/de:fortunes/en"
app.config['FORTUNE_PATH_DE'] = "fortunes/de"
app.config['FORTUNE_PATH_EN'] = "fortunes/en"

if 'GAID' in os.environ:
    app.config['GAID'] = os.environ['GAID']


class API(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('lang')
        parser.add_argument('categories')
        args = parser.parse_args()

        p = 'FORTUNE_PATH'

        if args['lang']:
            lang = str(args['lang']).upper()
            if lang == "DE" or lang == "EN":
                p = 'FORTUNE_PATH_'+lang
        path = app.config.get(p).split(":")

        f = path
        if args['categories']:
            f = []
            categories = args['categories'].split(",")
            for c in categories:
                f.append(path[0]+"/"+c)
            percentages, fortune_files = fortune.fortune_files_from_paths(app.config.get('FORTUNE_PATH').split(':'))
            for e in f:
                if  e not in fortune_files:
                    return {
                        "error": "Invalid combination of language and category",
                        "validelements": fortune_files
                    }

        cookie = fortune.get_random_fortune(
            f,
            offensive=False,
            weighted=False,
            max_length=300,

        ).rstrip()

        return {
            'fortune': cookie,
            'source': f,

        }


@app.route('/')
def index():
    cookie = fortune.get_random_fortune(
        app.config.get('FORTUNE_PATH').split(':'),
        offensive=False,
        weighted=False,
        max_length=300
    )
    return render_template('index.html', cookie=cookie, gaid=app.config.get('GAID'))


@app.route('/info')
def info():
    percentages, fortune_files = fortune.fortune_files_from_paths(app.config.get('FORTUNE_PATH').split(':'))

    selections = []
    for p in fortune_files:
        p = os.path.relpath(p, "fortunes")
        selections.append(p.split("/"))
    return render_template('info.html', selections=selections, gaid=app.config.get('GAID'))


api.add_resource(API, '/api/')

# [END gae_python37_app]